<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\BingoGame as MainController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [MainController::class, 'index'])->name('home');

Route::get('/generate-number', [MainController::class, 'callBingoNumber'])->name('callBingoNumber');

Route::get('/create-bingo-card', [MainController::class, 'createBingoCard'])->name('createBingoCard');