<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BingoCard extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'column_b_1',
        'column_b_2',
        'column_b_3',
        'column_b_4',
        'column_b_5',
        'column_i_1',
        'column_i_2',
        'column_i_3',
        'column_i_4',
        'column_i_5',
        'column_n_1',
        'column_n_2',
        'column_n_3',
        'column_n_4',
        'column_n_5',
        'column_g_1',
        'column_g_2',
        'column_g_3',
        'column_g_4',
        'column_g_5',
        'column_o_1',
        'column_o_2',
        'column_o_3',
        'column_o_4',
        'column_o_5',
    ];
}

